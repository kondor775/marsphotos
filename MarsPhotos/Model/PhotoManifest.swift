//
//  PhotoManifest.swift
//  MarsPhotos
//
//  Created by Kondor on 01/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation

class PhotoManifest: NSObject {
    let name: String
    let landingDate: Date?
    let launchDate: Date?
    let status: String
    let maxSol: Int
    let maxDate: Date?
    let totalPhotos: Int
    
    struct PhotoInManifest {
        let sol: Int
        let earthDate: Date?
        let totalPhotos: Int
        let cameras: [String]
    }
    
    let photos: [PhotoInManifest]
    
    required init?(with manifestDictionary: [String: Any]) {
        guard let photoManifestDictionary = manifestDictionary["photo_manifest"] as? [String: Any] else {
            return nil
        }
        
        self.name = photoManifestDictionary["name"] as! String
        
        if let landingDate = FormatterUtils.nasaDateFormatter.date(from: photoManifestDictionary["landing_date"] as! String) {
            self.landingDate = landingDate
        } else {
            self.landingDate = nil
        }
        
        if let launchDate = FormatterUtils.nasaDateFormatter.date(from: photoManifestDictionary["launch_date"] as! String) {
            self.launchDate = launchDate
        } else {
            self.launchDate = nil
        }
        
        self.status = photoManifestDictionary["status"] as! String
        self.maxSol = photoManifestDictionary["max_sol"] as! Int
        
        if let maxDate = FormatterUtils.nasaDateFormatter.date(from: photoManifestDictionary["max_date"] as! String) {
            self.maxDate = maxDate
        } else {
            self.maxDate = nil
        }
        
        self.totalPhotos = photoManifestDictionary["total_photos"] as! Int
        let photoInManifestArray = photoManifestDictionary["photos"] as! [[String: Any]]
        var photoInManifestStructsArray: [PhotoInManifest] = Array.init()
        
        for photoInManifest in photoInManifestArray {
            let sol: Int = photoInManifest["sol"] as! Int
            let earthDate: Date?
            
            if let earthParsedDate = FormatterUtils.nasaDateFormatter.date(from: photoInManifest["earth_date"] as! String) {
                earthDate = earthParsedDate
            } else {
                earthDate = nil
            }
            
            let totalPhotos: Int = photoInManifest["total_photos"] as! Int
            let cameras: [String] = photoInManifest["cameras"] as! [String]
            photoInManifestStructsArray.append(PhotoInManifest.init(sol: sol,
                                                                    earthDate: earthDate,
                                                                    totalPhotos: totalPhotos,
                                                                    cameras: cameras))
        }
        
        self.photos = photoInManifestStructsArray
    }
    
    static func mapper(with data: Data) -> Any? {
        if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            if let jsonObject: [String : Any] = jsonObject{
                return PhotoManifest.init(with: jsonObject)
            }
            
            return nil
        }
        
        return nil
    }
    
    func getSolsArray(with numberOfPhotos: Int) -> [Int] {
        var solsArray: [Int] = Array.init()
        var numberOfCollectedPhotos: Int = 0
        
        for photosIndex in stride(from: self.photos.count - 1, through: 0, by: -1) {
            let photoInManifest: PhotoInManifest = self.photos[photosIndex]
            solsArray.append(photoInManifest.sol)
            numberOfCollectedPhotos += photoInManifest.totalPhotos
            if numberOfCollectedPhotos >= numberOfPhotos {
                return solsArray.reversed()
            }
        }
        
        return solsArray.reversed()
    }
}
