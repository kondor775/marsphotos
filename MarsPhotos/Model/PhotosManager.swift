//
//  PhotosManager.swift
//  MarsPhotos
//
//  Created by Kondor on 02/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation
import CoreData

class PhotosManager: NSObject {
    static var sharedPM: PhotosManager = {
        let pmInstance = PhotosManager()
        return pmInstance
    }()
    
    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
    
    let kHiddenPhotosArray: String = "kHiddenPhotosArray"
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "MarsPhotos")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
                #warning("case handling needed")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                #warning("case handling needed")
            }
        }
    }
    
    private override init() {
        if let url = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask).last {
            print(url.absoluteString)
        }
        
    }
    
    private func createPhotoEntityFrom(dictionary: [String: Any]) -> NSManagedObject? {
        let context = self.persistentContainer.viewContext
        if let photoEntity = NSEntityDescription.insertNewObject(forEntityName: "Photo", into: context) as? Photo {
            photoEntity.setMetadata(with: dictionary)
            return photoEntity
        }
        return nil
    }
    
    func importPhotosMetadata(with metadataArray: [[String : Any]]) {
        for dict in metadataArray {
            _ = self.createPhotoEntityFrom(dictionary: dict)
        }
        saveContext()
    }
    
    func getImportedPhotosUrls() -> [String] {
        let context = self.persistentContainer.viewContext
        var photoUrls: [String] = [String]()
        
        do{
            let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
            let photoEntitys = try context.fetch(fetchRequest)
            for photoEntity: Photo in photoEntitys {
                if let photoUrl = photoEntity.img_src {
                    photoUrls.append(photoUrl)
                }
            }
            return photoUrls
        } catch {
            fatalError("Failed to execute request")
            #warning("case handling needed")
        }
    }
    
    private func updatePhotoEntityImageData(with url: String, data: Data) {
        let context = self.persistentContainer.viewContext
        
        do{
            let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "img_src = %@", url)
            let photoEntitys = try context.fetch(fetchRequest)
            
            for photoEntity: Photo in photoEntitys {
                photoEntity.setImageData(with: data)
            }
        } catch {
            fatalError("Failed to execute request")
            #warning("case handling needed")
        }
    }
    
    func importPhotosImageData(with photosDataList: [String : Any]) {
        for (url, data) in photosDataList {
            updatePhotoEntityImageData(with: url, data: data as! Data)
        }
        saveContext()
    }
    
    func hidePhoto(with id: Int64) {
        let context = self.persistentContainer.viewContext
        
        do{
            let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "id = %@", String(id))
            let photoEntitys = try context.fetch(fetchRequest)
            
            for photoEntity: Photo in photoEntitys {
                context.delete(photoEntity)
            }
            
            markPhotoAsHidden(with: id)
            
            saveContext()
        } catch {
            fatalError("Failed to execute request")
            #warning("case handling needed")
        }
    }
    
    func markPhotoAsHidden(with id: Int64) {
        var hiddenPhotosMutableArray: [String] = [String]()
        if let hiddenPhotosArray: [String] = getHiddenPhotosIDs() {
            hiddenPhotosMutableArray.append(contentsOf: hiddenPhotosArray)
        }
        hiddenPhotosMutableArray.append(String(id))
        UserDefaults.standard.set(hiddenPhotosMutableArray, forKey: kHiddenPhotosArray)
    }
    
    func getHiddenPhotosIDs() -> [String]? {
        if let hiddenPhotosArray: [String] = UserDefaults.standard.array(forKey: kHiddenPhotosArray) as? [String] {
            return hiddenPhotosArray
        }
        
        return nil
    }
    
    func clearAllData() {
        let context = self.persistentContainer.viewContext
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: Photo.fetchRequest())
        batchDeleteRequest.resultType = .resultTypeObjectIDs
        do {
            let result = try context.execute(batchDeleteRequest) as! NSBatchDeleteResult
            let changes: [AnyHashable: Any] = [
                NSDeletedObjectsKey: result.result as! [NSManagedObjectID]
            ]
            NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes, into: [context])
        } catch {
            fatalError("Failed to execute request")
            #warning("case handling needed")
        }
        
    }
}
