//
//  NetworkBaseClass.swift
//  MarsPhotos
//
//  Created by Kondor on 01/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation

class NetworkBaseClass: URLSession {
    static var sharedNBC: NetworkBaseClass = {
        let nbcInstance = NetworkBaseClass()
        return nbcInstance
    }()
    
    func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
    
    private struct NBCConstants {
        static let timeout: TimeInterval = 60
        static let maximumConnections: Int = 1
    }
    
    private enum NBCHTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case delete = "DELETE"
        case put = "PUT"
    }
    
    struct NBCError: Error {
        enum ErrorKind {
            case networkError
            case serverError
            case parsingError
            case unknownError
        }
        
        let errorCode: Int?
        let errorDescription: String?
        let kind: ErrorKind
        
        static func unknownError() -> NBCError {
            return NBCError.init(errorCode: nil,
                                 errorDescription: nil,
                                 kind: .unknownError)
        }
    }
    
    typealias nbcCompletionHandler = (Data?, URLResponse?, Error?) -> Void
    typealias nbcDataMapper = ((Data) -> Any?)?
    typealias nbcGeneralResponse = (Any?, NBCError?) -> Void
    
    var baseURL: URL?
    
    private let nbcDefaultURLSessionConfiguration: URLSessionConfiguration
    private let nbcDefaultURLSession: URLSession
    
    private override init() {
        nbcDefaultURLSessionConfiguration = URLSessionConfiguration.default
        nbcDefaultURLSessionConfiguration.timeoutIntervalForRequest = NBCConstants.timeout
        nbcDefaultURLSessionConfiguration.timeoutIntervalForResource = NBCConstants.timeout
        nbcDefaultURLSessionConfiguration.httpMaximumConnectionsPerHost = NBCConstants.maximumConnections
        nbcDefaultURLSession = URLSession.init(configuration: nbcDefaultURLSessionConfiguration)
    }
    
    private func getURLAppendix(with parameters: [String: String]?) -> String {
        var urlAppendix = String.init()
        guard let parameters = parameters else {
            return urlAppendix
        }
        
        var firstParameter: Bool = true;
        for (name, value) in parameters {
            let parameterPrefix = firstParameter ? "?" : "&"
            firstParameter = false
            urlAppendix.append("\(parameterPrefix)\(name)=\(value)")
        }
        
        return urlAppendix
    }
    
    private func getNBCDataTask(with session: URLSession, baseURL: URL?, methodURL: String, nbcHTTPMethod: NBCHTTPMethod, completionHandler: @escaping(nbcCompletionHandler)) -> URLSessionDataTask? {
        
        guard let encodedMethodURL = methodURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
              let requestUrl = URL(string: encodedMethodURL, relativeTo: baseURL) else {
                print("Error: cannot create URL")
                return nil
        }
        
        var urlRequest: URLRequest = URLRequest.init(url: requestUrl)
        urlRequest.httpMethod = nbcHTTPMethod.rawValue
        return session.dataTask(with: urlRequest, completionHandler: completionHandler)
    }
    
    func performNBCGetRequest(methodURL: String, parameters: [String: String]?, completionHandler: @escaping(nbcCompletionHandler)) {
        guard let nbcDataTask = getNBCDataTask(with: nbcDefaultURLSession, baseURL: baseURL, methodURL: methodURL.appending(getURLAppendix(with: parameters)), nbcHTTPMethod: .get, completionHandler: completionHandler) else {
            print("Error: cannot create nbcDataTask")
            #warning("case handling with completionHandler needed")
            return
        }
        
        nbcDataTask.resume()
    }
    
    func performNBCGetRequest(methodURL: String, completionHandler: @escaping(nbcCompletionHandler)) {
        performNBCGetRequest(methodURL: methodURL, parameters: nil, completionHandler: completionHandler)
    }
    
    static func getNASAApiErrorMessage(with data: Data) -> String? {
        if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            guard let jsonObject: [String : Any] = jsonObject,
                let jsonErrorDictionary = jsonObject["error"] as? [String: Any],
                let jsonErrorMessage = jsonErrorDictionary["message"] as? String else {
                    return nil
            }
            return jsonErrorMessage
        }
        
        return nil
    }
    
    static func generalResponseHandler(data: Data?, response: URLResponse?, error: NSError?, nbcDataMapper: nbcDataMapper,  generalResponse: @escaping(nbcGeneralResponse)) {
        if let error = error {
            let nbcError: NBCError = NBCError.init(errorCode: error.code,
                                                   errorDescription: error.localizedDescription,
                                                   kind: .networkError)
            DispatchQueue.main.async {
                generalResponse(nil,nbcError)
            }
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse else {
            let nbcError: NBCError = NBCError.init(errorCode: nil,
                                                   errorDescription: nil,
                                                   kind: .serverError)
            generalResponse(nil,nbcError)
            return
        }
        
        guard (200...299).contains(httpResponse.statusCode) else {
            let errorDescription: String?
            
            if let data = data, let errorMessage = getNASAApiErrorMessage(with: data) {
                errorDescription = errorMessage
            } else {
                errorDescription = nil
            }
            
            let nbcError: NBCError = NBCError.init(errorCode: httpResponse.statusCode,
                                                   errorDescription: errorDescription,
                                                   kind: .serverError)
            DispatchQueue.main.async {
                generalResponse(nil,nbcError)
            }
            return
        }
        
        if let data = data {
            guard let nbcDataMapper = nbcDataMapper else {
                DispatchQueue.main.async {
                    generalResponse(data,nil)
                }
                return
            }
            
            if let mappedObject = nbcDataMapper(data) {
                DispatchQueue.main.async {
                    generalResponse(mappedObject,nil)
                }
                return
            } else {
                let nbcError: NBCError = NBCError.init(errorCode: nil,
                                                       errorDescription: nil,
                                                       kind: .parsingError)
                DispatchQueue.main.async {
                    generalResponse(nil,nbcError)
                }
            }
        
        } else {
            let nbcError: NBCError = NBCError.init(errorCode: nil,
                                                   errorDescription: nil,
                                                   kind: .unknownError)
            DispatchQueue.main.async {
                generalResponse(nil,nbcError)
            }
        }
    }
    
}
