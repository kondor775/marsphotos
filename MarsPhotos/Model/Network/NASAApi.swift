//
//  NASAApi.swift
//  MarsPhotos
//
//  Created by Kondor on 01/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation

class NASAApi: NSObject {
    typealias photoManifestApiResponse = (PhotoManifest?, NetworkBaseClass.NBCError?) -> Void
    typealias photosMetadataListApiResponse = ([[String : Any]]?, NetworkBaseClass.NBCError?) -> Void
    typealias generalDataApiResponse = (Data?, NetworkBaseClass.NBCError?) -> Void
    typealias photoDataApiResponse = (Data?, String, NetworkBaseClass.NBCError?) -> Void
    typealias photosDataListApiResponse = ([String : Data]?, NetworkBaseClass.NBCError?) -> Void
    
    private struct nasaApiConstants {
        static let nasaApiBaseUrl: String = "https://api.nasa.gov/mars-photos/api/v1/"
        static let nasaApiKey: String = "DEMO_KEY"
        
        static let getPhotoManifestMethodUrl: String = "manifests/Curiosity"
        static let getPhotosListMethodUrl: String = "rovers/curiosity/photos"
    }
    
    static func getPhotoManifest(photoManifestApiResponse: @escaping photoManifestApiResponse) {
        let requestParameters = ["api_key": nasaApiConstants.nasaApiKey]
        
        NetworkBaseClass.sharedNBC.baseURL = URL(string: nasaApiConstants.nasaApiBaseUrl)
        NetworkBaseClass.sharedNBC.performNBCGetRequest(methodURL: nasaApiConstants.getPhotoManifestMethodUrl,
                                                        parameters: requestParameters,
                                                        completionHandler: { (data, response, error) in
                                                            NetworkBaseClass.generalResponseHandler(data: data, response: response, error: error as NSError?, nbcDataMapper: PhotoManifest.mapper, generalResponse: { (mappedObject, nbcError) in
                                                                photoManifestApiResponse(mappedObject as? PhotoManifest, nbcError)
                                                            })
        })
        
    }
    
    static func getPhotosMetadataList(with sol: Int, photosMetadataListApiResponse: @escaping photosMetadataListApiResponse) {
        let requestParameters = ["api_key": nasaApiConstants.nasaApiKey, "sol": String(sol)]
        
        NetworkBaseClass.sharedNBC.baseURL = URL(string: nasaApiConstants.nasaApiBaseUrl)
        NetworkBaseClass.sharedNBC.performNBCGetRequest(methodURL: nasaApiConstants.getPhotosListMethodUrl,
                                                        parameters: requestParameters,
                                                        completionHandler: { (data, response, error) in
                                                            NetworkBaseClass.generalResponseHandler(data: data, response: response, error: error as NSError?, nbcDataMapper: PhotosUtils.photosMetadataMapper, generalResponse: { (mappedObject, nbcError) in
                                                                photosMetadataListApiResponse(mappedObject as? [[String : Any]], nbcError)
                                                            })
        })
        
    }
    
    static func getPhotosMetadataList(with sols: [Int], photosMetadataListApiResponse: @escaping photosMetadataListApiResponse) {
        let requestsDispatchGroup = DispatchGroup()
        var resultPhotosList: [[String : Any]]? = Array.init()
        
        for sol: Int in sols {
            requestsDispatchGroup.enter()
            self.getPhotosMetadataList(with: sol, photosMetadataListApiResponse: { (photosList, error) in
                if let error = error {
                    resultPhotosList = nil
                    photosMetadataListApiResponse(nil, error)
                } else if let photosList: [[String : Any]] = photosList {
                    resultPhotosList!.append(contentsOf: photosList)
                } else {
                    photosMetadataListApiResponse(nil, error)
                }
                requestsDispatchGroup.leave()
            })
        }
        
        requestsDispatchGroup.notify(queue: .main) {
            print("NOTIFY")
            if let resultPhotosList: [[String : Any]] = resultPhotosList {
                photosMetadataListApiResponse(resultPhotosList, nil)
            }
        }
        
    }
    
    static func getPhotoData(with url: String, photoDataApiResponse: @escaping photoDataApiResponse) {
        
        NetworkBaseClass.sharedNBC.baseURL = nil
        NetworkBaseClass.sharedNBC.performNBCGetRequest(methodURL: url,
                                                        completionHandler: { (data, response, error) in
                                                            NetworkBaseClass.generalResponseHandler(data: data, response: response, error: error as NSError?, nbcDataMapper: nil, generalResponse: { (data, nbcError) in
                                                                photoDataApiResponse(data as? Data, url, nbcError)
                                                            })
        })
        
    }
    
    static func getPhotosDataList(with urls: [String], photosDataListApiResponse: @escaping photosDataListApiResponse) {
        let requestsDispatchGroup = DispatchGroup()
        var resultPhotosDataList: [String : Data]? = Dictionary.init()
        
        for url: String in urls {
            print("enter\(url)")
            requestsDispatchGroup.enter()
            self.getPhotoData(with: url, photoDataApiResponse: { (data, url, error) in
                if let error = error {
                    print("Error: \(error)")
                    resultPhotosDataList = nil
                    photosDataListApiResponse(nil, error)
                } else if let photoData: Data = data {
                    resultPhotosDataList![url] = photoData
                } else {
                    photosDataListApiResponse(nil, error)
                }
                requestsDispatchGroup.leave()
            })
        }
        
        requestsDispatchGroup.notify(queue: .main) {
            print("NOTIFY")
            if let resultPhotosDataList: [String : Data] = resultPhotosDataList {
                photosDataListApiResponse(resultPhotosDataList, nil)
            }
        }
        
    }
    
}
