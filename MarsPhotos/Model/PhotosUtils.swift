//
//  PhotosUtils.swift
//  MarsPhotos
//
//  Created by Kondor on 02/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation

class PhotosUtils: NSObject {
    static func photosMetadataMapper(with data: Data) -> Any? {
        if let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
            guard let jsonObject: [String : Any] = jsonObject, let jsonPhotoDictionary = jsonObject["photos"] as? [[String: Any]] else {
                    return nil
                }
                return jsonPhotoDictionary
            }
            
            return nil
    }
}
