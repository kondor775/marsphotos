//
//  PhotoExtension.swift
//  MarsPhotos
//
//  Created by Kondor on 02/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation

extension Photo {
    func setMetadata(with photoDictionary: [String: Any]) {
        self.id = photoDictionary["id"] as! Int64
        self.sol = photoDictionary["sol"] as! Int16
        self.img_src = (photoDictionary["img_src"] as! String)
    }
    
    func setImageData(with imageData: Data) {
        self.img_data = imageData
    }
}
