//
//  CDDataSource.swift
//  MarsPhotos
//
//  Created by Kondor on 04/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import UIKit
import CoreData

class CDDataSource: NSObject, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    let tableView: UITableView
    let managedObjectContext: NSManagedObjectContext
    let photosManager: PhotosManager
    
    required init(with managedObjectContext: NSManagedObjectContext, tableView: UITableView, photosManager: PhotosManager) {
        self.tableView = tableView
        self.managedObjectContext = managedObjectContext
        self.photosManager = photosManager
    }
    
    // MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionInfo = fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let photoTableViewCell: PhotoTableViewCell = tableView.dequeueReusableCell(withIdentifier: PhotoTableViewCell.photoTableViewCellIdentifier, for: indexPath) as! PhotoTableViewCell
        let photo = fetchedResultsController.object(at: indexPath)
        PhotoTableViewCellConfigurator.configure(photoTableViewCell, forDisplaying: photo)
        
        return photoTableViewCell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    // MARK: - Fetched results controller
    
    var fetchedResultsController: NSFetchedResultsController<Photo> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<Photo> = Photo.fetchRequest()
        
        fetchRequest.fetchBatchSize = GlobalConstants.numberOfPhotosToGet
        
        if let hiddenPhotosIDs = photosManager.getHiddenPhotosIDs() {
            fetchRequest.predicate = NSPredicate(format: "NOT (id IN %@)", hiddenPhotosIDs)
        }
        
        let sortDescriptorBySol = NSSortDescriptor(key: "sol", ascending: true)
        let sortDescriptorById = NSSortDescriptor(key: "id", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptorBySol, sortDescriptorById]
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        aFetchedResultsController.delegate = self
        _fetchedResultsController = aFetchedResultsController
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            #warning("case handling needed")
        }
        
        return _fetchedResultsController!
    }
    var _fetchedResultsController: NSFetchedResultsController<Photo>? = nil
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(IndexSet(integer: sectionIndex), with: .fade)
        case .delete:
            tableView.deleteSections(IndexSet(integer: sectionIndex), with: .fade)
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            if let photoTableViewCell = tableView.cellForRow(at: indexPath!) as? PhotoTableViewCell {
                PhotoTableViewCellConfigurator.configure(photoTableViewCell, forDisplaying: anObject as! Photo)
            }
        case .move:
            if let photoTableViewCell = tableView.cellForRow(at: indexPath!) as? PhotoTableViewCell {
                PhotoTableViewCellConfigurator.configure(photoTableViewCell, forDisplaying: anObject as! Photo)
            }
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

}
