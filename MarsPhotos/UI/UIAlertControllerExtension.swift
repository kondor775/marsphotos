//
//  UIAlertControllerExtension.swift
//  MarsPhotos
//
//  Created by Kondor on 04/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation
import UIKit

extension UIAlertController {
    private static let title: String = "Getting NASA images error"
    
    static func present(with error: NetworkBaseClass.NBCError, viewContoller: UIViewController?) {
        var fullMessage: String = String()
        
        switch error.kind {
            case .networkError:
                fullMessage.append("Network error\n")
            case .serverError:
                fullMessage.append("Server error\n")
            case .parsingError:
                fullMessage.append("Parsing error\n")
            case .unknownError:
                fullMessage.append("Unknown error\n")
        }
        
        if let errorCode = error.errorCode {
            fullMessage.append("Code: \(errorCode)\n")
        }
        
        if let errorDescription = error.errorDescription {
            fullMessage.append("Description: \(errorDescription)\n")
        }
        
        let alertController = UIAlertController(title: title, message: fullMessage, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel))
        
        if let viewContoller = viewContoller {
            viewContoller.present(alertController, animated: true, completion: nil)
        } else {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                while let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                
                topController.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
}
