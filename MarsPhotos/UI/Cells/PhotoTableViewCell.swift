//
//  PhotoTableViewCell.swift
//  MarsPhotos
//
//  Created by Kondor on 04/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {
    
    static let photoTableViewCellIdentifier: String = "PhotoTableViewCell"
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var solLabel: UILabel!
    var photoId: Int64!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }

}
