//
//  PhotoTableViewCellConfigurator.swift
//  MarsPhotos
//
//  Created by Kondor on 04/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation
import UIKit

class PhotoTableViewCellConfigurator {
    static func configure(_ photoTableViewCell: PhotoTableViewCell, forDisplaying photo: Photo) {
        photoTableViewCell.photoId = photo.id
        photoTableViewCell.solLabel.text = "sol: \(photo.sol.description)"
        
        if let imageData = photo.img_data, let image = UIImage(data: imageData) {
            photoTableViewCell.photoImageView.image = image
        } else {
            photoTableViewCell.photoImageView.image = nil
        }
    }
}
