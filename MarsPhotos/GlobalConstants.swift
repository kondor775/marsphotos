//
//  GlobalConstants.swift
//  MarsPhotos
//
//  Created by Kondor on 03/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation

struct GlobalConstants {
    static let numberOfPhotosToGet: Int = 25
}
