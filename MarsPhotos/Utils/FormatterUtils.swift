//
//  FormatterUtils.swift
//  MarsPhotos
//
//  Created by Kondor on 01/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import Foundation

class FormatterUtils: NSObject {
    static var nasaDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD"
        return dateFormatter
    }()
    
    private override init() {}
}
