//
//  ImageViewController.swift
//  MarsPhotos
//
//  Created by Kondor on 04/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    
    var photo: Photo!
    @IBOutlet weak var zoomableImageView: GTZoomableImageView!
    @IBOutlet weak var noImageLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let imageData = photo.img_data, let image = UIImage(data: imageData) {
            zoomableImageView.image = image
        } else {
            noImageLabel.isHidden = false
        }

        
    }

}
