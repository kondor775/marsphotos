//
//  MainViewController.swift
//  MarsPhotos
//
//  Created by Kondor on 03/10/2019.
//  Copyright © 2019 Aleksey Sirotkin. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var photoTableView: UITableView!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    
    var dataSource: CDDataSource!
    var longPressedCellPhotoid: Int64? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = CDDataSource(with: PhotosManager.sharedPM.persistentContainer.viewContext,
                                  tableView: self.photoTableView,
                                  photosManager: PhotosManager.sharedPM)
        photoTableView.dataSource = dataSource
        photoTableView.delegate = self
        
        downloadImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func downloadImages() {
        waitCompletion(true)
        
        NASAApi.getPhotoManifest(photoManifestApiResponse:{ [weak self] (photoManifest, error) in
            if let error = error {
                self?.waitCompletion(false)
                UIAlertController.present(with: error, viewContoller: self)
                return
            }
            
            guard let solsArray = photoManifest?.getSolsArray(with: GlobalConstants.numberOfPhotosToGet),
                solsArray.count > 0 else {
                    self?.waitCompletion(false)
                    UIAlertController.present(with: NetworkBaseClass.NBCError.unknownError(), viewContoller: self)
                    return
            }
            
            NASAApi.getPhotosMetadataList(with: solsArray, photosMetadataListApiResponse: { [weak self] (photosList, error) in
                if let error = error {
                    self?.waitCompletion(false)
                    UIAlertController.present(with: error, viewContoller: self)
                    return
                }
                
                guard let photosList = photosList else {
                    self?.waitCompletion(false)
                    UIAlertController.present(with: NetworkBaseClass.NBCError.unknownError(), viewContoller: self)
                    return
                }
                
                PhotosManager.sharedPM.clearAllData()
                
                let limitedPhotosList = Array(photosList.suffix(GlobalConstants.numberOfPhotosToGet))
                PhotosManager.sharedPM.importPhotosMetadata(with: limitedPhotosList)
                
                let importedPhotosUrls = PhotosManager.sharedPM.getImportedPhotosUrls()
                
                NASAApi.getPhotosDataList(with: importedPhotosUrls, photosDataListApiResponse: { [weak self] (dataWithUrlDictionary, error) in
                    if let error = error {
                        self?.waitCompletion(false)
                        UIAlertController.present(with: error, viewContoller: self)
                        return
                    }
                    
                    guard let dataWithUrlDictionary = dataWithUrlDictionary else {
                        self?.waitCompletion(false)
                        UIAlertController.present(with: NetworkBaseClass.NBCError.unknownError(), viewContoller: self)
                        return
                    }
                    
                    PhotosManager.sharedPM.importPhotosImageData(with: dataWithUrlDictionary)
                    
                    self?.displayImages()
                    self?.waitCompletion(false)
                })
            })
            
        })
    }
    
    func displayImages() {
        photoTableView.reloadData()
    }
    
    func waitCompletion(_ wait: Bool) {
        waitingView.isHidden = !wait
        refreshButton.isEnabled = !wait
    }
    
    @IBAction func refreshButtonAction(_ sender: UIButton) {
        downloadImages()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let gestureRecognizers = cell.gestureRecognizers, gestureRecognizers.count != 0 else {
            let tableViewCellGestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPressOnCell(sender:)))
            cell.addGestureRecognizer(tableViewCellGestureRecognizer)
            return
        }
    }
    
    @objc func handleLongPressOnCell(sender: UILongPressGestureRecognizer) {
        if sender.state == .ended {
            if let tappedCell = sender.view as? PhotoTableViewCell {
                longPressedCellPhotoid = tappedCell.photoId
                tappedCell.becomeFirstResponder()
                let hideMenuItem = UIMenuItem(title: "Hide this photo", action: #selector(hidePhotoTap(sender:)))
                let menuController = UIMenuController.shared
                if let tappedCellSuperview = tappedCell.superview {
                    menuController.setTargetRect(tappedCell.frame, in: tappedCellSuperview)
                }
                menuController.menuItems = [hideMenuItem]
                menuController.setMenuVisible(true, animated: true)
            }
        }
    }
    
    @objc func hidePhotoTap(sender: UIMenuController) {
        if let longPressedCellPhotoid = self.longPressedCellPhotoid {
            PhotosManager.sharedPM.hidePhoto(with: longPressedCellPhotoid)
        }
        
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "imageViewControllerSegue" {
            if let indexPath = photoTableView.indexPathForSelectedRow {
                let object = dataSource.fetchedResultsController.object(at: indexPath)
                let imageViewController = segue.destination as! ImageViewController
                imageViewController.photo = object
            }
        }
    }
    
}
